# Ambiente Logo

Se trata de una extensión para la plataforma de desarrollo **App inventor** que incropora un ambiente de programación bajo el paradigma **Gráficos de tortuga** creado por Seymour Papert.

Contenido del repositorio:

* Extensión `Logo`.
* Plantilla para proyectos compatible con servidores **App Inventor**
* Documentación.
* Galería de ejemplos.

Trabajo en proceso:

* Generador de código tipo Byte-code.
* Intérprete (Plataforma Android).

Enlaces:

[Documentación](https://gitlab.com/gdeldago/ambiente-logo/wikis/Ambiente-Logo.)

[Galería de ejemplos](https://gitlab.com/gdeldago/ambiente-logo/wikis/Galer%C3%ADa-de-ejemplos.)

[Descargas](https://gitlab.com/gdeldago/ambiente-logo)

[Reportes sobre errores y sugerencias](https://gitlab.com/gdeldago/ambiente-logo/issues)
