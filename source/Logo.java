 /*
    Copyright (C) 2018 Gustavo Del Dago (gdeldago@fundacionsadosky.org.ar)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package edu.tusl.gus.logo;

import com.google.appinventor.components.runtime.*;
import com.google.appinventor.components.annotations.DesignerComponent;
import com.google.appinventor.components.annotations.DesignerProperty;
import com.google.appinventor.components.annotations.PropertyCategory;
import com.google.appinventor.components.annotations.SimpleFunction;
import com.google.appinventor.components.annotations.SimpleObject;
import com.google.appinventor.components.annotations.SimpleProperty;
import com.google.appinventor.components.common.ComponentCategory;
import com.google.appinventor.components.common.PropertyTypeConstants;
import com.google.appinventor.components.common.YaVersion;
import com.google.appinventor.components.runtime.errors.YailRuntimeError;
import com.google.appinventor.components.*;
import android.app.Activity;
import android.util.Log;
import java.lang.NumberFormatException;
import java.lang.Math;
import java.util.ArrayList;
import java.util.*;

    
@DesignerComponent(version = YaVersion.LABEL_COMPONENT_VERSION,
    description = "<b>Ambiente LOGO</b>."
                + "<p>(C) 2018 Gustavo Del Dago (gdeldago@fundacionsadosky.org.ar)</p>"
                + "<p><a ref=https://gitlab.com/gdeldago/ambiente-logo>https://gitlab.com/gdeldago/ambiente-logo</a></p>",
    category = ComponentCategory.EXTENSION,
    nonVisible = true,
    iconName = "aiwebres/logo.png")

@SimpleObject(external = true)

public class Logo extends AndroidNonvisibleComponent {

    private static final String LOG_TAG = "Logo";
  
    // Posición
    private double x = 0;			
    private double y = 0;

    // Rumbo
    private int angulo = 90;

    // Escenario
    private Canvas lienzo = null;
    private int ancho;
    private int alto;
    
    // Actor
    private Sprite actor = null;
    
    // Estado
    private boolean conPluma = true;
    private boolean actorVisible = true;

    //
    // Constructor
    //
    public Logo(ComponentContainer container) {
        super(container.$form());
    }

    //
    // Primitiva Inicializar
    //
    @SimpleFunction(description = "Permite indicar sobre que lienzo dibujará el autómata Logo.")
    public void Inicializar(Canvas lienzo) {
        //if (!(lienzo instanceof Canvas)) {
        //    throw new IllegalArgumentError("¿Que hacemo papá?");
        //}
        this.lienzo = lienzo;
        this.ancho = lienzo.Width();
        this.alto = lienzo.Height();
        this.Centro();
    }

    //
    // Primitiva Actor
    //
    @SimpleFunction(description = "Permite modificar la apariencia del autómata Logo.")
    public void Actor(Sprite sprite) {
        //if (!(lienzo instanceof Canvas)) {
        //    throw new IllegalArgumentError("Sprite constructor called with container " + container);
        //}
        this.actor = sprite;
    }

    //
    // Primitiva FijarColorDeFondo
    //
    @SimpleFunction(description = "Permite fijar el color de fondo del escenario.")
    public void FijarColorDeFondo(int color) {
        this.lienzo.BackgroundColor(color);
    }

    //
    // Primitiva FijarColorDePluma
    //
    @SimpleFunction(description = "Permite fijar el color de la pluma.")
    public void FijarColorDePluma(int color) {
        this.lienzo.PaintColor(color);
    }

    //
    // Primitiva ConPluma
    //
    @SimpleFunction(description = "Indica al autómata que baje su pluma (dibuja al desplazarce).")
    public void ConPluma() {
        this.conPluma = true;
    }

    //
    // Primitiva SinPluma
    //
    @SimpleFunction(description = "Indica al autómata que alce su pluma (no dibuja al desplazarce).")
    public void SinPluma() {
        this.conPluma = false;
    }

    //
    // Primitiva Limpiar
    //
    @SimpleFunction(description = "Limpia el escenario.")
    public void Limpiar() {
        this.lienzo.Clear();
    }

    //
    // Primitiva MostrarActor
    //
    @SimpleFunction(description = ".")
    public void MostarActor() {
        this.actorVisible = true;
        this.actor.Visible(this.actorVisible);
    }

    //
    // Primitiva OcultarActor
    //
    @SimpleFunction(description = ".")
    public void OcultarActor() {
        this.actorVisible = false;
        this.actor.Visible(this.actorVisible);
    }

    //
    // Primitiva Centro
    //
    @SimpleFunction(description = "Establece la ubicación del autómata en centro del escenario (0,0) y fija su rumbo en dirección norte (0)")
    public void Centro() {
        this.x = this.ancho / 2;
        this.y = this.alto / 2;
        this.angulo = 90;
        this.moverActor();
        this.rotarActor();
    }

    //
    // Primitiva Adelante
    //
    @SimpleFunction(description = "Hace avanzar al autómata Logo una cantidad de pasos.")
    public void Adelante(double pasos) {

        double x1, x2, y1, y2;
        double anguloEnRadianes = Math.toRadians(angulo);

        x1 = this.x;
        y1 = this.y;

        x2 = x1 + (pasos * Math.cos(anguloEnRadianes));
        y2 = y1 - (pasos * Math.sin(anguloEnRadianes));

        if (x2 > this.ancho) {
            x2 = this.ancho;
        }

        if (x2 < 0) {
            x2 = 0;
        }

        if (y2 > this.alto) {
            y2 = this.alto;
        }

        if (y2 < 0) {
            y2 = 0;
        }

        if (conPluma) {
            this.lienzo.DrawLine((int)x1, (int)y1, (int)x2, (int)y2);
        }

        this.x = x2;
        this.y = y2;

        this.moverActor();
    }

    //
    // Primitiva Atras
    //
    @SimpleFunction(description = "Hace retroceder al autómata Logo una cantidad de pasos.")
    public void Atras(double pasos) {
        this.Adelante(pasos * -1);
    }

    //
    // Primitiva Izquierda
    //
    @SimpleFunction(description = "Hace girar al autómata hacia su izquiera una cantidad de grados.")
    public void Izquierda(int grados) {
        this.angulo = (this.angulo + grados) % 360;
        this.rotarActor();
    }

    //
    // Primitiva Derecha
    //
    @SimpleFunction(description = "Hace girar al autómata hacia su derecha una cantidad de grados.")
    public void Derecha(int grados) {
        this.angulo = (this.angulo - grados) % 360;
        this.rotarActor();    	
    }

    //
    // Primitiva Escribir
    //
    @SimpleFunction(description = "Escribe texto en escenario.")
    public void Escribir(String texto) {
        this.lienzo.DrawTextAtAngle(texto, (int)this.x, (int)this.y, this.angulo);
    }

    //
    // Primitiva Pintar
    //
    @SimpleFunction(description = "Pinta un área cerrada del lienzo con el color de la pluma. El autómata Logo deberá estar dentro del área.")
    public void Pintar() {
        int colorObjetivo = this.lienzo.GetBackgroundPixelColor((int)this.x, (int)this.y);
        int colorNuevo = this.lienzo.PaintColor();

        this.Rellenar((int)this.x, (int)this.y, colorObjetivo, colorNuevo);
    }

    //
    // Primitiva FijarRumbo
    //
    @SimpleFunction(description = "Fija el rumbo del autómata Logo.")
    public void FijarRumbo(int grados) {
        this.angulo = grados % 360;
        this.rotarActor();
    }

    //
    // Primitiva Rumbo
    //
    @SimpleFunction(description = "Retorna el rumbo actual del autómata Logo.")
    public int Rumbo() {
        return this.angulo;
    }

    //
    // Primitiva ColorDeFondo
    //
    @SimpleFunction(description = "Retorna el color de fondo del escenario (lienzo).")
    public int ColorDeFondo() {
        return this.lienzo.BackgroundColor();
    }

    //
    // Primitiva ColorDePluma
    //
    @SimpleFunction(description = "Retorna el color de la pluma.")
    public int ColorDePluma() {
        return this.lienzo.BackgroundColor();
    }

    //
    // Primitiva ColorDebajo
    //
    @SimpleFunction(description = "Retorna el color del escenario en el punto donde se encuentra el autómata Logo.")
    public int ColorDebajo() {
        return this.lienzo.GetBackgroundPixelColor((int)this.x, (int)this.y);
    }

    //
    // RellenarRecursiva (Obsoleta). 
    //
    // Implementa el algoritmo Flood-fill. 
    //
    // NOTA: La versión recursiva es elegante pero poco eficiente. 
    //       En Android no hay memoria suficiente :)
    //       
    /*
    private void RellenarRecursiva(int x, int y, int colorObjetivo, int colorNuevo) {

        // Si no estamos dentro del lienzo, retornar.
        if ((x < 0) || (x > this.ancho)) {
            return;
        }
        if ((y < 0) || (y > this.alto)) {
            return;
        }
        // Si el colorObjetivo es igual al colorNuevo, retornar.
        if (colorObjetivo == colorNuevo)
            return;

        // Si el color del punto actual no es igual al colorObjetivo, retornar.
        if (this.lienzo.GetBackgroundPixelColor(x, y) != colorObjetivo)
            return;

        this.lienzo.SetBackgroundPixelColor(x, y, colorNuevo);
        // Procesamos un punto hacia el Norte. 
        RellenarRecursiva(x, y-1, colorObjetivo, colorNuevo);
        // Procesamos un punto hacia el Sur. 
        RellenarRecursiva(x, y+1, colorObjetivo, colorNuevo);
        // Procesamos un punto hacia el Oeste. 
        RellenarRecursiva(x-1, y, colorObjetivo, colorNuevo);
        // Procesamos un punto hacia el Este. 
        RellenarRecursiva(x+1, y, colorObjetivo, colorNuevo);
    }
    */

    //
    // Rellenar. 
    //
    // Implementa el algoritmo Flood-fill. Los puntos a procesar se almacenan en una estructura tipo cola.
    //
    private void Rellenar(int x, int y, int colorObjetivo, int colorNuevo) {
        int o;
        int e;

        // Si no estamos dentro del lienzo, retornar.
        if ((x < 0) || (x > this.ancho)) {
            return;
        }
        if ((y < 0) || (y > this.alto)) {
            return;
        }
        // Si el colorObjetivo es igual al colorNuevo, retornar.
        if (colorObjetivo == colorNuevo) {
            return;
        }
        // Si el color del punto actual no es igual al colorObjetivo, retornar.
        if (this.lienzo.GetBackgroundPixelColor(x, y) != colorObjetivo) {
            return;
        }

        // Inicializamos una cola para almacenar los puntos pendientes de pintar.
        Queue<Integer> queue = new LinkedList<Integer>();

        // agregamos a la cola las coordenadas del punto actual.
        queue.add(x); 
        queue.add(y);

        // Mientras haya elemntos en la cola, los procesamos.
        while (!queue.isEmpty()) {
            // Obtenemos un punto de la cola.
            x = queue.remove(); 
            y = queue.remove();
            // Control de los recorridos hacia Oeste y Este 
            o = x; 
            e = x;

            // Nos movemos hacia el Oeste mientras los puntos sean del colorObjetivo.
            while (this.lienzo.GetBackgroundPixelColor(o, y) == colorObjetivo) {
                this.lienzo.SetBackgroundPixelColor(o, y, colorNuevo);
                // Si el color al Norte del punto actual es del colorObjetivo, agregamos ese punto a la cola.
                if (this.lienzo.GetBackgroundPixelColor(o, y + 1) == colorObjetivo) {
                    queue.add(o); 
                    queue.add(y+1);
                }
                // Si el color al Sur del punto actual es del colorObjetivo, agregamos ese punto a la cola.
                if (this.lienzo.GetBackgroundPixelColor(o, y - 1) == colorObjetivo) {
                    queue.add(o); 
                    queue.add(y-1);
                }
                o--;
            }
            // Nos movemos hacia el Oeste mientras los puntos sean del colorObjetivo.
            e++;
            while (this.lienzo.GetBackgroundPixelColor(e, y) == colorObjetivo) {
                this.lienzo.SetBackgroundPixelColor(e, y, colorNuevo);
                // Si el color al Norte del punto actual es del colorObjetivo, agregamos ese punto a la cola.
                if (this.lienzo.GetBackgroundPixelColor(e, y + 1) == colorObjetivo) {
                    queue.add(e); 
                    queue.add(y+1);
                }
                // Si el color al Sur del punto actual es del colorObjetivo, agregamos ese punto a la cola.
                if (this.lienzo.GetBackgroundPixelColor(e, y - 1) == colorObjetivo) {
                    queue.add(e); 
                    queue.add(y-1);
                }
                e++;
            }
        }
    }


    //
    // moverActor.
    //
    // Invoca a los métodos del componente Sprite
    //
    private void moverActor() {
        double actor_x = this.x - this.actor.Width() / 2;
        double actor_y = this.y - this.actor.Height() / 2;

        this.actor.MoveTo(actor_x, actor_y);
    }

    //
    // rotarActor.
    //
    // Invoca a los métodos del componente Sprite
    //
    private void rotarActor() {
        this.actor.Heading(this.angulo - 90);
    }

}
